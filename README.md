# Silent Edge

Najkraće rečeno, fastpaced LAN multiplayer PvP topdown shooter/fighter igrica.

Članovi:
 - <a href="https://gitlab.com/lukastan1">Luka Stanković 38/2020</a>
 - <a href="https://gitlab.com/pecelin">Jovan Milanović 405/2021</a>
 - <a href="https://gitlab.com/Stefann2">Stefan Nikolić 124/2020</a>
 - <a href="https://gitlab.com/sandraugrinic">Sandra Ugrinić 266/2017</a>
 - <a href="https://gitlab.com/djordje1298">Đorđe Marinković 378/2020</a>

  ## Izgradnja projekta

 - Klonirati repozitorijum
 - Otvoriti dobijeni folder u QtCreator-u
 - Pritisnuti na build dugme
